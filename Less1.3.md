# Less1.3
## Итак необходимо заставить работать ls и cp.
Проделали следующие манипуляции чтобы столкнуться с тем что ls и cp не будут работать:<br/>
```bash
root@juneway-work-machine:/home/catware# mkdir less3
root@juneway-work-machine:/home/catware# mkdir less3/bin
root@juneway-work-machine:/home/catware# ls less3/
bin
```
![chroot](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_7.jpg)<br/>
Затем копируем bash из / в наш новый корень /less3:
```bash
root@juneway-work-machine:/home/catware# cp /bin/bash less3/bin/
root@juneway-work-machine:/home/catware# chroot less3
chroot: failed to run command ‘/bin/bash’: No such file or directory
```
![bashnevzletaet](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_1.jpg)<br/>
Баш не взлетит потому что нужны динамические либы, которых естественно в нашем каталоге нет. Через ldd можно посмотреть чего не хватает башу:<br/>
```bash
root@juneway-work-machine:/home/catware# ldd less3/bin/bash
        linux-vdso.so.1 (0x00007fff880a3000)
        libtinfo.so.6 => /lib/x86_64-linux-gnu/libtinfo.so.6 (0x00007f15648e2000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f15648dc000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f1564707000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f1564a50000)
```
![chegonexvataet](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_2.jpg)<br/>
Копируем не достающие либы из / в less3/lib или less3/lib64:<br/>
```bash
root@juneway-work-machine:/home/catware# mkdir less3/lib
root@juneway-work-machine:/home/catware# mkdir less3/lib64
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libtinfo.so.6 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libdl.so.2 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libc.so.6 less3/lib
root@juneway-work-machine:/home/catware# cp /lib64/ld-linux-x86-64.so.2 less3/lib64
```
![cplib](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_3.jpg)<br/>
Теперь можно выполнить chroot:<br/>
```bash
root@juneway-work-machine:/home/catware# chroot less3
bash-5.1# ls
bash: ls: command not found
bash-5.1# cp
bash: cp: command not found
```
![cpnotfound](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_5.jpg)<br/>
Команды не заработают, потому что их точно также надо будет скопировать, но уже из /usr/bin/ls и /usr/bin/cp соответстветственно:<br/>
```bash
root@juneway-work-machine:/home/catware# mkdir less3/usr
root@juneway-work-machine:/home/catware# mkdir less3/usr/bin
root@juneway-work-machine:/home/catware# cp /usr/bin/ls less3/usr/bin
root@juneway-work-machine:/home/catware# cp /usr/bin/cp less3/usr/bin
```
![cplsandcp](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_6.jpg)<br/>
Но этого недостаточно:<br/>
![nepomozhet](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_8.jpg)<br/>
Надо через ldd посмотреть чего не хватает и скопировать не достающее:<br/>
```bash
bash-5.1# exit
exit
root@juneway-work-machine:/home/catware# ldd less3/usr/bin/ls
        linux-vdso.so.1 (0x00007ffc6316d000)
        libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f839e35f000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f839e18a000)
        libpcre2-8.so.0 => /lib/x86_64-linux-gnu/libpcre2-8.so.0 (0x00007f839e0f2000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f839e0ec000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f839e3b7000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f839e0ca000)
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libselinux.so.1 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libc.so.6 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libpcre2-8.so.0 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libpcre2-8.so.0 less3/lib
root@juneway-work-machine:/home/catware# cp /lib64/ld-linux-x86-64.so.2 less3/lib64
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libpthread.so.0 less3/lib
root@juneway-work-machine:/home/catware# chroot less3
bash-5.1# ls
bin  lib  lib64  usr
```
![ls](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_9.jpg)<br/>
Вуаля, ls заработал! Теперь для команды cp:<br/>
```bash
bash-5.1# exit
exit
root@juneway-work-machine:/home/catware# ldd less3/usr/bin/cp
        linux-vdso.so.1 (0x00007fff267ba000)
        libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f650b211000)
        libacl.so.1 => /lib/x86_64-linux-gnu/libacl.so.1 (0x00007f650b206000)
        libattr.so.1 => /lib/x86_64-linux-gnu/libattr.so.1 (0x00007f650b1fe000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f650b029000)
        libpcre2-8.so.0 => /lib/x86_64-linux-gnu/libpcre2-8.so.0 (0x00007f650af91000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f650af89000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f650b269000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f650af67000)
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libselinux.so.1 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libacl.so.1 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libattr.so.1 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libc.so.6 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libpcre2-8.so.0 less3/lib
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libdl.so.2 less3/lib
root@juneway-work-machine:/home/catware# cp /lib64/ld-linux-x86-64.so.2 less3/lib64
root@juneway-work-machine:/home/catware# cp /lib/x86_64-linux-gnu/libpthread.so.0 less3/lib
```
![cpcp](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_10.jpg)<br/>
Все прекрасно скопировалось, теперь создадим пустой файл через touch чтобы посмотреть копируется ли файл из под chroot:<br/>
```bash
root@juneway-work-machine:/home/catware# touch less3/1.txt
root@juneway-work-machine:/home/catware# chroot less3
bash-5.1# ls -la
total 24
drwxr-xr-x 6 0 0 4096 Jan 29 16:03 .
drwxr-xr-x 6 0 0 4096 Jan 29 16:03 ..
-rw-r--r-- 1 0 0    0 Jan 29 16:03 1.txt
drwxr-xr-x 2 0 0 4096 Jan 29 15:21 bin
drwxr-xr-x 2 0 0 4096 Jan 29 16:00 lib
drwxr-xr-x 2 0 0 4096 Jan 29 15:32 lib64
drwxr-xr-x 3 0 0 4096 Jan 29 15:39 usr
bash-5.1# cp 1.txt /bin
bash-5.1# ls -la /bin
total 1216
drwxr-xr-x 2 0 0    4096 Jan 29 16:05 .
drwxr-xr-x 6 0 0    4096 Jan 29 16:03 ..
-rw-r--r-- 1 0 0       0 Jan 29 16:05 1.txt
-rwxr-xr-x 1 0 0 1234376 Jan 29 15:21 bash
```
![cpisworkingnormally](https://gitlab.com/juneway16/less1.3/-/raw/main/Screenshot_11.jpg)<br/>
## В итоге все работает. По сути все свелось к копироваю самих программ ls и cp и копированию недостающих либ в наш каталог. Такие дела.